﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TowerButtonClick : MonoBehaviour {
    public GameObject player;
    public bool isButtonArmed;
    public Button button;
    public GameObject towerAssigned;
    private Vector3 mouseLoc;
    private float costToBuild;
	// Use this for initialization
	void Start () {
        Button btn = button.GetComponent<Button>();
        isButtonArmed = false;
        btn.onClick.AddListener(TaskOnClick);
        costToBuild = towerAssigned.GetComponentInChildren<TurretStats>().turretCost;
	}
	
	// Update is called once per frame
	void Update () {
        if (isButtonArmed)
        {
            if (Input.GetMouseButtonUp(1))
            {
                if (player.GetComponent<PlayerStats>().MoneyCheck(costToBuild))
                {
                    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                    RaycastHit hit;
                    if (Physics.Raycast(ray, out hit, 100))
                    {
                        Instantiate(towerAssigned, hit.point, Quaternion.identity);
                    }
                    isButtonArmed = false;
                }
                else {
                    Debug.Log("Not enough Gold!!!");
                }
            }
        }
        else {
            //nothing happens
        }
	}

    void TaskOnClick() {
        isButtonArmed = true;
    }
}
