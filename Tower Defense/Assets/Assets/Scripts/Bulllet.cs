﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bulllet : MonoBehaviour {
	public GameObject tower;
	private float damage;
    private float lifetime = 3.0f;
	void OnTriggerEnter(Collider other){
		if(other.gameObject.tag == "Enemy" || other.gameObject.tag == "Flyer"){
			other.gameObject.GetComponent<EnemyStats>().GetDamaged(damage);
			Destroy(this.gameObject);
		}
	}
	void Start(){
		damage = tower.GetComponent<TurretStats>().towerDamage;
        Destroy(this.gameObject, lifetime);
	}
    void Update() {
        //Destroy(this.gameObject, lifetime);
    }
}
