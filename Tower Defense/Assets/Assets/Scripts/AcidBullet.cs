﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AcidBullet : MonoBehaviour {
	public GameObject tower;
	private float damage;
	//for dbug purposes
	public float slowValue;
	void OnTriggerEnter(Collider other){
		if(other.gameObject.tag == "Enemy"){
			other.gameObject.GetComponent<EnemyStats>().GetDamaged(damage);
			other.gameObject.GetComponent<EnemyStats>().enemySpeed -= slowValue;
			Destroy(this.gameObject);
		}
	}
	// Use this for initialization
	void Start () {
		damage = tower.GetComponent<TurretStats>().towerDamage;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
