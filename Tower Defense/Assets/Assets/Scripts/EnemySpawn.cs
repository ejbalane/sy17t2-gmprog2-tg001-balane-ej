﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawn : MonoBehaviour {
	//Minions to be spawned
	public List<GameObject> enemies = new List<GameObject>();
	//Boss to be spawned
	public GameObject boss;
	//Delay between wave spawning
	public float spawnTimer;
	//Number of enemies to be spawned on waves
	public int unitCount;
	//Number of waves before boss spawns
	public int bossCount;
	//Restart boss counter
	public int defaultBossCountdown;

	public List<GameObject> spawnedEnemies = new List<GameObject>();
	//Checks if we can spawn next wave
	public int totalWaveCount = 0;
	GoalManager goalManager;
	// Use this for initialization
	void Start () {
		defaultBossCountdown = bossCount;
		goalManager = FindObjectOfType<GoalManager>();
	}
	public void DestroyEnemy(GameObject enemy){
		spawnedEnemies.Remove(enemy);
		Destroy(enemy);
	}
	//Spawns enemies
	IEnumerator Spawn(int enemyCount){
		for(int i = 0; i < enemyCount; i++){
			if(goalManager.GetGoalHP()){
				int randomIndex = Random.Range(0,enemies.Count);
				GameObject randomEnemy = Instantiate(enemies[randomIndex], transform.position, Quaternion.identity) as GameObject;
				EnemyStats enemyStats = randomEnemy.GetComponent<EnemyStats>();
			
			
				if(totalWaveCount > 1){
					if(enemyStats.enemyType == EnemyStats.ENEMYTYPE.boss){
						enemyStats.maxHP *= 2.0f;
						enemyStats.enemyGold *= 1.5f;
						enemyStats.enemyHP = enemyStats.maxHP;
					} else{
						enemyStats.maxHP += GetComponent<EnemyScale>().increaseEnemyHP * totalWaveCount;
						enemyStats.enemyHP = enemyStats.maxHP;
						enemyStats.enemyGold += GetComponent<EnemyScale>().increaseEnemyGold * totalWaveCount;
					}
				}
				spawnedEnemies.Add(randomEnemy);
			}
			yield return new WaitForSeconds(0.01f);
		}
	}
	void ResetBossCountdown(){
		bossCount = defaultBossCountdown;
	}
	void startEnemyWave(){
		if(bossCount > 0){
			StartCoroutine(Spawn(unitCount));
			bossCount -= 1;
		} else {
			GameObject randomEnemy = Instantiate(boss, transform.position, Quaternion.identity) as GameObject;
			StartCoroutine(Spawn(5));
			spawnedEnemies.Add(randomEnemy);
			ResetBossCountdown();
		}
		totalWaveCount += 1;
	}
	
	// Update is called once per frame
	void Update () {
		if(!goalManager.GetGoalHP()){
			return;
		}
		if(spawnedEnemies.Count<=0){
			startEnemyWave();
		}
		/*if(GameObject.FindGameObjectsWithTag("Enemy").Length <= 0){
			startEnemyWave();
		}*/
	}
}
