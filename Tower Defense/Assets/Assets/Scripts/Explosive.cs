﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosive : MonoBehaviour {
	public GameObject cannonBall;
	private float damage;
	void OnTriggerEnter(Collider other){
		if(other.gameObject.tag == "Enemy"){
			other.gameObject.GetComponent<EnemyStats>().GetDamaged(damage);
		}
	}
	// Use this for initialization
	void Start () {
		damage = cannonBall.GetComponent<CannonBall>().explosionDamage;
	}
}
