﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyMovement : MonoBehaviour {
	public Transform goalTransform;
	public GameObject goalGameObject;
	// Use this for initialization
	void Start () {
		NavMeshAgent agent = GetComponent<NavMeshAgent>();
		agent.speed = this.gameObject.GetComponent<EnemyStats>().enemySpeed;
		agent.destination = goalGameObject.transform.position;
		if(agent.speed < 0.0f){
			agent.speed = 0.0f;
		}
	}
}
