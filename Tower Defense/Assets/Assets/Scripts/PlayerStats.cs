﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerStats : MonoBehaviour {
    public Text goldDisplay;
	public float playerGold = 500.0f;
	// Use this for initialization
	void Start () {
        //ToDisplay();
        playerGold = 500.0f;
    }
	public bool MoneyCheck(float cost){
		if(playerGold - cost < 0){
			return false;
		}
		playerGold -= cost;
        ToDisplay();
		return true;
	}
	public void AddGold(EnemyStats enemyStats){
		playerGold += enemyStats.enemyGold;
        ToDisplay();
	}
	// Update is called once per frame
	void Update () {
        ToDisplay();
	}
    void ToDisplay() {
        goldDisplay.text = "Player Gold: <b>" + playerGold.ToString() + "</b>";
    }
}
