﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour {
	public float panSpeed = 20f;
	public float edgeBuffer = 10f;
	public float scrollSpeed;
	public float xMin;
	public float xMax;
	public float zMin;
	public float zMax;
	public float yMin;
	public float yMax;
	
	// Update is called once per frame
	void Update () {
		Vector3 cameraPosition = this.transform.position;
		if(Input.GetKey("w") || Input.mousePosition.y >= Screen.height - edgeBuffer){
			cameraPosition.z += panSpeed * Time.deltaTime;
		}
		if(Input.GetKey("s") || Input.mousePosition.y <= edgeBuffer){
			cameraPosition.z -= panSpeed * Time.deltaTime;
		}
		if(Input.GetKey("a") || Input.mousePosition.x <= edgeBuffer){
			cameraPosition.x -= panSpeed * Time.deltaTime;
		}
		if(Input.GetKey("d") || Input.mousePosition.x >= Screen.width - edgeBuffer){
			cameraPosition.x += panSpeed * Time.deltaTime;
		}
		
		cameraPosition.x = Mathf.Clamp(cameraPosition.x,xMin,xMax);
		cameraPosition.z = Mathf.Clamp(cameraPosition.z,zMin,zMax);

		float scroll = Input.GetAxis("Mouse ScrollWheel");
		cameraPosition.y -= scroll * scrollSpeed * 100f * Time.deltaTime;
		
		cameraPosition.y = Mathf.Clamp(cameraPosition.y,yMin,yMax);
		
		transform.position = cameraPosition;
	}
}
