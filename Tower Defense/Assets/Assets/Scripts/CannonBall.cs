﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CannonBall : MonoBehaviour {
    public GameObject tower;
    public GameObject explosive;
    public float explosionDamage;
    public bool explode;
    private float lifetime;
    //explosion must not exceed limiter
    public float explosion;
    //explosion limiter
    public float explosionLimiter;
    void OnTriggerEnter(Collider other) {
        if (other.gameObject.tag == "Enemy") {
            explode = true;
        }
    }
	// Use this for initialization
	void Start () {
        explode = false;
        explosionDamage = tower.GetComponent<TurretStats>().towerDamage;
        
	}
	
	// Update is called once per frame
	void Update () {
        if (explode) {
            explosive.GetComponent<SphereCollider>().radius += explosion;
        }
        if (explosive.GetComponent<SphereCollider>().radius >= explosionLimiter) {
            Destroy(this.gameObject);
        }
        //Destroy(this.gameObject, lifetime);
    }
}
