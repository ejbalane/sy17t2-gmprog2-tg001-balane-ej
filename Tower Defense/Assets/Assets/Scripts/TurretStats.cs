﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretStats : MonoBehaviour {
	public float towerDamage;
	public float turretCost;
	public float turretFireRate;
	public float buildTime;
	public enum TOWERLEVEL{
		basic,
		advanced,
		legendary
	}
	public TOWERLEVEL tOWERLEVEL;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
