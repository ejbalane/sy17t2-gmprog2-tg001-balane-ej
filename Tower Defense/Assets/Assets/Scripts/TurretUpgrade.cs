﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretUpgrade : MonoBehaviour {
    public float increaseFireRate = 1.0f;
    public float increaseTurretDmg = 10.0f;
}
