﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeteorCollider : MonoBehaviour {
    public float damage;
    public MeteorScript script;
    public Vector3 target;
    public float speed;
    void OnTriggerEnter(Collider other) {
        if (other.gameObject.tag == "Enemy")
        {
            damage = other.gameObject.GetComponent<EnemyStats>().enemyHP * 0.85f;
            other.gameObject.GetComponent<EnemyStats>().GetDamaged(damage);
            //Destroy(this.gameObject);
        }
        else if (other.gameObject.tag == "Terrain") {
            Destroy(this.gameObject);
        }
        else if(other.gameObject.tag == "Indicator") {
            Destroy(other.gameObject);
            Destroy(this.gameObject);
        }
    }
	// Use this for initialization
	void Start () {
        
        script = FindObjectOfType<MeteorScript>();
        target = script.targetPos;
        speed = script.meteorSpeed;
	}
	
	// Update is called once per frame
	void Update () {
        transform.position = Vector3.MoveTowards(this.transform.position, target, speed * Time.deltaTime);
        
	}
}
