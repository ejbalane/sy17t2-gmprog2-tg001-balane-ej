﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MeteorCooldownTimer : MonoBehaviour {
    public Text timer;
    MeteorScript meteor;

    public float currentCountdown = 5.0f;
	// Use this for initialization
	void Start () {
        meteor = GetComponent<MeteorScript>();
        currentCountdown = 0.0f;
	}
    public void SetTimer(float time) {
        time = (int)Mathf.Clamp(time, 0, meteor.coolDown);
        timer.text = "Meteor: " + time;
    }
    public bool SkillArmed() {
        if (currentCountdown <= 0.0f)
        {
            return true;
        }
        else {
            return false;
        }
    }
    public void StartTimer() {
        currentCountdown = meteor.coolDown;
    }
	// Update is called once per frame
	void Update () {
        currentCountdown -= Time.deltaTime;
        currentCountdown = Mathf.Max(0, currentCountdown);
        SetTimer(currentCountdown);
	}
}
