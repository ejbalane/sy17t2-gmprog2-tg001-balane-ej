﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ReaperScytheCooldownTimer : MonoBehaviour {
    public Text timer;
    ReaperScytheScript reaper;

    public float currentCooldown = 5.0f;
	// Use this for initialization
	void Start () {
        reaper = GetComponent<ReaperScytheScript>();
        currentCooldown = 0.0f;
	}
    public void SetTimer(float time) {
        time = (int)Mathf.Clamp(time, 0, reaper.coolDown);
        timer.text = "ReaperScythe: " + time;
    }
    public bool SkillArmed() {
        if (currentCooldown <= 0.0f)
        {
            return true;
        }
        else {
            return false;
        }
    }
    public void StartTimer() {
        currentCooldown = reaper.coolDown;
    }
	// Update is called once per frame
	void Update () {
        currentCooldown -= Time.deltaTime;
        currentCooldown = Mathf.Max(0, currentCooldown);
        SetTimer(currentCooldown);
	}
}
