﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ReaperScytheScript : Skills {
    public Button reaperButton;
    public EnemySpawn spawn;
    public ReaperScytheCooldownTimer cooldownTimer;
    public List<GameObject> possibleTargets;
    public float scytheLimit;
    public GameObject scythe;
    public float damageTobeDealt;
	// Use this for initialization
	void Start () {
        Button btn = reaperButton.GetComponent<Button>();
        btn.onClick.AddListener(TaskToDo);
        possibleTargets = spawn.spawnedEnemies;
        btn.interactable = false;
	}

    void TaskToDo() {
        if (cooldownTimer.SkillArmed()) {
            foreach (var enemy in possibleTargets) {
                if (enemy == null) {
                    continue;
                }
                EnemyStats enemyStats = enemy.GetComponent<EnemyStats>();
                //TODO: check if target is still alive based on stats
                if (enemyStats != null) {
                    //TODO: make a target discriminant to discriminate targets from one another based on stats
                    float targetDiscriminant = enemyStats.maxHP * 0.15f;
                    //TODO: follow through logic from requirement
                    if (enemyStats.enemyHP <= targetDiscriminant) {
                        damageTobeDealt = enemyStats.enemyHP * 0.25f;
                        //TODO: insert expanding collider here
                        Instantiate(scythe, enemy.transform.position, Quaternion.identity);
                        scythe.GetComponent<ScytheScript>().expand = true;
                        return;
                    }
                }
            }
            cooldownTimer.StartTimer();
        }
    }
	
	// Update is called once per frame
	void Update () {
		TaskToDo();
	}
}
