﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScytheScript : MonoBehaviour {
    public GameObject Reaper;
    private float damage;
    public bool expand;
    public float limit;
    void OnTriggerEnter(Collider other) {
        if (other.gameObject.tag == "Enemy") {
            other.gameObject.GetComponent<EnemyStats>().GetDamaged(damage);
        }
    }
	// Use this for initialization
	void Start () {
        damage = Reaper.GetComponent<ReaperScytheScript>().damageTobeDealt;
        limit = Reaper.GetComponent<ReaperScytheScript>().scytheLimit;
	}
    void Update() {
        if (expand) {
            this.GetComponent<SphereCollider>().radius += limit;
        }
        if (this.GetComponent<SphereCollider>().radius >= limit) {
            Destroy(this.gameObject);
        }
    }
}
