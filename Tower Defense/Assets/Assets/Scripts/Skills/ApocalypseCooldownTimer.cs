﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ApocalypseCooldownTimer : MonoBehaviour {
    public Text timer;
    ApocalypseScript apocalypse;

    public float currentCountdown = 5.0f;

    void Start() {
        apocalypse = FindObjectOfType<ApocalypseScript>();
        currentCountdown = 0.0f;
    }

    public void SetTimer(float time) {
        time = (int)Mathf.Clamp(time, 0, apocalypse.coolDown);
        timer.text = "Apocalypse: " + time;
    }

    public bool SkillArmed() {
        if (currentCountdown <= 0.0f)
        {
            return true;
        }
        else {
            return false;
        }
        //return currentCountdown <= 0.0f;
    }

    public void StartTimer() {
        currentCountdown = apocalypse.coolDown;
    }

    void Update() {
        currentCountdown -= Time.deltaTime;
        currentCountdown = Mathf.Max(0, currentCountdown);
        SetTimer(currentCountdown);
    }
}
