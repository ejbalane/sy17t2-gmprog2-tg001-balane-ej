﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MeteorScript : Skills {
    public Button meteorButton;
    public bool isButtonArmed;
    public bool isMeteorReady;
    MeteorCooldownTimer cooldownTimer;
    public GameObject indicator;
    public GameObject meteorSpawnPosition;
    public GameObject meteor;
    public float meteorSpeed;
    public Vector3 targetPos;
    // Use this for initialization
    void Start () {
        Button btn = meteorButton.GetComponent<Button>();
        btn.onClick.AddListener(TaskToDo);
        cooldownTimer = GetComponent<MeteorCooldownTimer>();
        isButtonArmed = false;
        btn.interactable = true;
        isMeteorReady = false;
	}
    void TaskToDo() {
        isButtonArmed = true;
        if (isButtonArmed)
        {
            if (cooldownTimer.SkillArmed())
            {
                isMeteorReady = true;
                Instantiate(indicator, meteorSpawnPosition.transform.position, Quaternion.identity);
                //Instantiate(indicator, )
            }
            else
            {
                //Nothing Happens
            }
        }
        else
        {
            //Nothing Happens
        }
    }
	
	// Update is called once per frame
	void Update () {
        if(cooldownTimer.SkillArmed()){
            meteorButton.GetComponent<Button>().interactable = true;
        } else {
            meteorButton.GetComponent<Button>().interactable = false;
        }
        if (Input.GetMouseButtonUp(1)) {
            if (isMeteorReady) {
                if (Input.GetMouseButtonUp(1))
                {
                    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                    RaycastHit hit;
                    if (Physics.Raycast(ray, out hit, 100))
                    {
                        Instantiate(meteor, meteorSpawnPosition.transform.position, Quaternion.identity);
                        targetPos = hit.point;
                        //meteor.transform.position = Vector3.MoveTowards(meteor.transform.position, hit.point, meteorSpeed * Time.deltaTime);
                    }
                    isButtonArmed = false;isMeteorReady = false;
                    cooldownTimer.StartTimer();
                }               
                else {
                    //Nothing Happenms
                }
            }
        }
    }
}
