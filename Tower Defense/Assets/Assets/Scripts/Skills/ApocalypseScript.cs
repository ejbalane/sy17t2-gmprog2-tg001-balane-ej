﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ApocalypseScript : Skills {
    public Button apocalypseButton;
    public EnemySpawn spawn;
    ApocalypseCooldownTimer cooldownTimer;
    public List<GameObject> targets;
	// Use this for initialization
	void Start () {
        Button btn = apocalypseButton.GetComponent<Button>();
        btn.onClick.AddListener(TasktoDo);
        targets = spawn.spawnedEnemies;
        cooldownTimer = GetComponent<ApocalypseCooldownTimer>();
        btn.interactable = true;
	}
	
	// Update is called once per frame
	void Update () {
		if(cooldownTimer.SkillArmed()){
            apocalypseButton.GetComponent<Button>().interactable = true;
        } else {
            apocalypseButton.GetComponent<Button>().interactable = false;
        }
    }
    void TasktoDo() {
        if (cooldownTimer.SkillArmed())
        {
            foreach (var enemy in targets)
            {
                EnemyStats stats;
                stats = enemy.GetComponent<EnemyStats>();

                stats.GetDamaged(9999.0f);
            }
            cooldownTimer.StartTimer();
        }
    }
}
