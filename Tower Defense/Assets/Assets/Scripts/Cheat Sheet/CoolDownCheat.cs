﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CoolDownCheat : MonoBehaviour {
    public Button cooldownButton;
    public ApocalypseCooldownTimer apocalypse;
    public MeteorCooldownTimer meteor;
    public ReaperScytheCooldownTimer scythe;
    public float set = 0.0f;
	// Use this for initialization
	void Start () {
        Button btn = cooldownButton.GetComponent<Button>();
        btn.onClick.AddListener(TaskToDo);
    }
    void TaskToDo() {
        apocalypse.currentCountdown = set;
        meteor.currentCountdown = set;
        scythe.currentCooldown = set;
    }
	// Update is called once per frame
	void Update () {
		
	}
}
