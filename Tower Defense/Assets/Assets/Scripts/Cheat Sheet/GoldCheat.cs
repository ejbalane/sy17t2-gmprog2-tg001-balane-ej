﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GoldCheat : MonoBehaviour {
	public Button goldButton;
    public GameObject player;
    public float addGold = 500.0f;
	// Use this for initialization
	void Start () {
		Button btn = goldButton.GetComponent<Button>();
		btn.onClick.AddListener(TaskToDo);
	}
	void TaskToDo(){
		player.GetComponent<PlayerStats>().playerGold += addGold;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
