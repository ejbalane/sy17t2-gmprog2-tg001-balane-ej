﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class KillCheat : MonoBehaviour {
    public Button killButton;
    public EnemySpawn spawn;
    public List<GameObject> targets;
	// Use this for initialization
	void Start () {
        Button btn = killButton.GetComponent<Button>();
        btn.onClick.AddListener(TaskToDo);
        targets = spawn.spawnedEnemies;
	}
    void TaskToDo() {
        foreach (var enemy in targets) {
            EnemyStats enemyStats;
            enemyStats = enemy.GetComponent<EnemyStats>();
            enemyStats.GetDamaged(9999.0f);
        }
    }
}
