﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Turret : MonoBehaviour {
	public Transform enemyInRange;
	public bool enemyDetected;
	private Transform initialTransform;
	public GameObject muzzlePoint;
	public GameObject bullet;
	public bool allowedToFire;
	TurretStats turretStats;
	public float fireRate;
    public float fireCountdown = 1.0f;
	public bool isTowerShooting = false;
	void OnTriggerEnter(Collider other){
		if(other.gameObject.tag != "Enemy"){
			//Nothing Happens
		} else{
			//Debug.Log("Enemy Detected");
			enemyDetected = true;
			enemyInRange = other.gameObject.transform;
			transform.LookAt(enemyInRange);
			
		}
	}
	void OnTriggerExit(){
		enemyDetected = false;
        allowedToFire = false;
	}
	// Use this for initialization
	void Start () {
        enemyInRange = null;
		allowedToFire = false;
		fireRate = turretStats.GetComponent<TurretStats>().turretFireRate;
		InvokeRepeating("Shoot", 10.0f, fireRate);
	}
	
	// Update is called once per frame
	void Update () {
		if(enemyInRange != null){
			if(Vector3.Distance(this.transform.position, enemyInRange.transform.position) < 10.0f){
				Vector3 relativePosition = enemyInRange.position - this.transform.position;
				Quaternion rotation = Quaternion.LookRotation(relativePosition);
				transform.rotation = rotation;
				//TODO: Bullet Instatiate (Debug)
				allowedToFire = true;
			} else {
				//Nothing Happens
				allowedToFire = false;
				enemyInRange = null;
				CancelInvoke();
			}
		} else {
			//Nothing Happens
		}
		if(allowedToFire){
            if (fireCountdown <= 0.0f){
                Shoot();
                fireCountdown = 1.0f / fireRate;
            }
            fireCountdown -= Time.deltaTime;
		}
	}

	void Shoot(){
		var go = Instantiate(bullet,muzzlePoint.transform.position, Quaternion.identity);
		go.GetComponent<Rigidbody>().AddRelativeForce(muzzlePoint.transform.forward * 1000.0f);
	}
}
