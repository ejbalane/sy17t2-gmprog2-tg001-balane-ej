﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GoalManager : MonoBehaviour {
	public float health;
	public EnemySpawn spawn;
    public Text goalHealth;
    void Start() {
        HealthDisplay();
    }
	public void OnTriggerEnter(Collider other){
        if (other.gameObject.tag == "Enemy")
        {
            health -= other.GetComponent<EnemyStats>().enemyCoreDamage;
            spawn.DestroyEnemy(other.gameObject);

            Debug.Log("Enemy got through");
        }
        else {
            //Nothing Else can damage the sword
        }
	}
	public bool GetGoalHP(){
		return health > 0;
	}
    void HealthDisplay() {
        goalHealth.text = "Goal Health Remaining: <b>" + health.ToString() + "</b>";
    }
    void Update (){
        HealthDisplay();
    }
}
