﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStats : MonoBehaviour {
	public float enemyHP;
	public float enemySpeed;
	public float enemyCoreDamage;
	public float enemyGold;
	public EnemySpawn spawn;
    public PlayerStats playerStats;
	public GameObject player;


	public enum ENEMYTYPE{
		flying,
		ground,
		boss
	}
	public ENEMYTYPE enemyType;

	public float maxHP;
	bool isDead;

	// Use this for initialization
	void Start () {
        playerStats = FindObjectOfType<PlayerStats>();
		isDead = false;
		spawn = FindObjectOfType<EnemySpawn>();
		enemyHP = maxHP;
        
	}

	public void GetDamaged(float damage){
		enemyHP -= damage;
	}
	
	// Update is called once per frame
	void Update () {
		if(isDead){
			return;
		}
		if(enemyHP <= 0){
			isDead = true;
            UponDeath();
			spawn.DestroyEnemy(this.gameObject);
            
		}
	}
	public void UponDeath(){
        playerStats.AddGold(this);
	}
}
