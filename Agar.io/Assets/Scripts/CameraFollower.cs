﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollower : MonoBehaviour {
	public Transform player;
	public Vector3 CameraOffset;
	public Vector3 CameraOrthoZoom;
	private float zoomSpeed = 5.0f;
	private float scaleX;
	// Use this for initialization
	void Start () {
		scaleX = player.transform.localScale.x;
	}
	
	// Update is called once per frame
	void Update () {
		this.transform.position = new Vector3 (player.position.x + CameraOffset.x, player.position.y + CameraOffset.y, player.position.z + CameraOffset.z);
		if(player.transform.localScale.x > scaleX){
			transform.position = Vector3.MoveTowards (transform.position, CameraOrthoZoom, zoomSpeed * Time.deltaTime);
		}
	}
}
