﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyDetection : MonoBehaviour {
    public Transform nearbyEnemy;
    private void OnTriggerEnter(Collider other)
    {
        Debug.Log(other.name);        
        nearbyEnemy = other.gameObject.transform;
        GetComponentInParent<EnemyScript>().enemytoFollow = nearbyEnemy;
        
    }
    private void OnTriggerExit(Collider other) {
        GetComponentInParent<EnemyScript>().enemytoFollow = null;
    }
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
