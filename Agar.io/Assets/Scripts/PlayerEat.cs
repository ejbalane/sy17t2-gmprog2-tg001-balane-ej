﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerEat : MonoBehaviour {
	public Text textScore;
	public int score;
	public float Add;
	public string SceneName;
	void OnTriggerEnter(Collider other){
		if(other.gameObject.transform.localScale.x < this.gameObject.transform.localScale.x){
			this.transform.localScale += new Vector3 (Add,Add,Add);
			Debug.Log ("Eat");
			score += 15;
			textScore.text = "Score:" + score;
			Destroy (other.gameObject);
		} else if(other.gameObject.transform.localScale.x > this.gameObject.transform.localScale.x){
            Debug.Log("You have Died!");
            Destroy(this.gameObject);
			SceneManager.LoadScene(SceneName);
        } else if(other.gameObject.transform.localScale.x == this.gameObject.transform.localScale.x){
			//Do Nothing
		}		
	}
	// Update is called once per frame
	void Update () {
		if(Input.GetKeyUp(KeyCode.Equals)){
			transform.localScale += new Vector3 (Add, Add, Add);
		}
		if(Input.GetKeyUp(KeyCode.Minus)){
			transform.localScale -= new Vector3 (Add, Add, Add);
		}
	}
}
