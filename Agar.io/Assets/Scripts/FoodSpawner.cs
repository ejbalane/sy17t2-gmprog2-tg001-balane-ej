﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FoodSpawner : MonoBehaviour {
	public GameObject FoodPrefab;
	public float repeatRate;
    public float spawnFirst = 30.0f;

	void GenerateFood(){
		int x = Random.Range (-50, 50);
		int y = Random.Range (-50, 50);
		Vector3 Target = Camera.main.ScreenToWorldPoint (new Vector3(x,y,-10));
		Instantiate (
			FoodPrefab,
			new Vector3(Random.Range(-50, 50),Random.Range(-50, 50), -10),
			Quaternion.identity
		);
	}
	// Use this for initialization
	void Start () {
		for (int i = 0; i <= spawnFirst; i++) {
			Instantiate (
				FoodPrefab,
				new Vector3(Random.Range(-50, 50),Random.Range(-50, 50), -10),
				Quaternion.identity
			);
		}
		InvokeRepeating ("GenerateFood", 0, repeatRate);
	}

	// Update is called once per frame
	void Update () {

	}
}
