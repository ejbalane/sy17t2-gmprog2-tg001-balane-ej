﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoadSceneOnClick : MonoBehaviour {
	public Button Play;
	public string SceneName;
	// Use this for initialization
	void Start () {
		Button btn = Play.GetComponent<Button> ();
		btn.onClick.AddListener (WhenClicked);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void WhenClicked(){
		SceneManager.LoadScene (SceneName);
	}
}
