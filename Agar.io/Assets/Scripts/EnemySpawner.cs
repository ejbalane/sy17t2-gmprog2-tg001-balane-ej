﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawner : MonoBehaviour {
	public GameObject enemyPrefab;
	public float repeatRate;
	void GenerateEnemy(){
		int x = Random.Range (-50,50);
		int y = Random.Range (-50, 50);
		Vector3 spawnPositions = Camera.main.ScreenToWorldPoint (new Vector3(x,y,-10));
		Instantiate (
			enemyPrefab, 
			new Vector3 (Random.Range (-50, 50), Random.Range (-50, 50), -10), 
			Quaternion.identity
		);
	}
	// Use this for initialization
	void Start () {
		InvokeRepeating ("GenerateEnemy", 0, repeatRate);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
