﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FiniteStateMachine : MonoBehaviour {
	public enum FiniteStateMachine_State{
		Patrol,
		Attack,
		Run
	}
	public FiniteStateMachine.FiniteStateMachine_State currentState;
	public FiniteStateMachine.FiniteStateMachine_State previousState;
	protected virtual void Initialize(){

	}
	protected virtual void FiniteStateMachineUpdate(){
		
	}
	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {

	}
}
