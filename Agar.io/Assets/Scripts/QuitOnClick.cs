﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuitOnClick : MonoBehaviour {
	public Button QuitButton;

	// Use this for initialization
	void Start () {
		Button btn = QuitButton.GetComponent<Button> ();
		btn.onClick.AddListener (WhenClicked);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void WhenClicked(){
		Application.Quit ();
	}
}
