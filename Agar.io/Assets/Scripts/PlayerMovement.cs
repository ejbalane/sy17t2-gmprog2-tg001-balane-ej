﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour {
	//public GameObject Player;
	//public Transform player;
	//Constraints
	private float minimumX = -50.0f;
	private float maximumX =  50.0f;
	private float minimumY = -50.0f;
	private float maximumY =  50.0f;

	public float speed = 5.0f;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		Vector3 playerPosition = transform.position;
		playerPosition.x = Mathf.Clamp (playerPosition.x, minimumX, maximumX);
		playerPosition.y = Mathf.Clamp (playerPosition.y, minimumY, maximumY);

		Vector3 gotoPoint = Camera.main.ScreenToWorldPoint (Input.mousePosition);

		gotoPoint.z = playerPosition.z;

		transform.position = Vector3.MoveTowards (playerPosition, gotoPoint, speed*Time.deltaTime/transform.localScale.x);
	}
}
