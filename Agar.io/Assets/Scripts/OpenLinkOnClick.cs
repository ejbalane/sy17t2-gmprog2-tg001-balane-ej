﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OpenLinkOnClick : MonoBehaviour {
	public Button LinkButton;
	// Use this for initialization
	void Start () {
		Button btn = LinkButton.GetComponent<Button> ();
		btn.onClick.AddListener (WhenClicked);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	void WhenClicked(){
		Application.OpenURL ("wolfpuppet.artstation.com");
		Application.Quit ();
	}
}
