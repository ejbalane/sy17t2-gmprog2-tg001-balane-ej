﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyScript : FiniteStateMachine {
	public float range;
	public Vector3 target;
	private float speed = 4.5f;
	//public float addtoScale = 0.1f;
	public Vector3 scaleAdd = new Vector3 (0.1f,0.1f,0.1f);
	public Vector3 Scale;
	public Transform enemytoFollow;
	// Use this for initialization
	void Start () {
		Initialize();
		gameObject.transform.localScale = Scale;
        //adjust the speed when growing
        SpeedChange();
	}
	// Update is called once per frame
	void Update () {
		FiniteStateMachineUpdate();
	}
	protected override void Initialize(){
		previousState = FiniteStateMachine.FiniteStateMachine_State.Patrol;
		currentState = FiniteStateMachine.FiniteStateMachine_State.Patrol;
	}
	protected override void FiniteStateMachineUpdate(){
		switch (currentState) {
		case FiniteStateMachine_State.Patrol:
                //Update Patrol
                UpdatePatrolState();
			break;
		case FiniteStateMachine_State.Attack:
                //Update Attack
                UpdateAttackState();
			break;
		case FiniteStateMachine_State.Run:
                //Update Run
                UpdateRunState();
			break;
		}
	}
	protected void UpdatePatrolState(){
		float num = Vector3.Distance (transform.position, target);
		if(num <= 5.0f){
            //Get new Patrol Point
            GetNewPatrolPoint();
		} if(previousState != currentState){
			previousState = currentState;
		}
		//Debug.Log ("I am Patrolling");
		transform.position = Vector3.MoveTowards(transform.position, target, speed * Time.deltaTime);
		if(enemytoFollow != null){
			currentState = FiniteStateMachine.FiniteStateMachine_State.Attack;
		}
	}
	void GetNewPatrolPoint(){
        target = new Vector3(Random.Range(-range, range), Random.Range(-range, range), -10.0f);
	}
    protected void UpdateAttackState() {
        if (previousState != currentState) {
            previousState = currentState;
        }
        if (enemytoFollow == null) {
            enemytoFollow = null;
            GetNewPatrolPoint();
            currentState = FiniteStateMachine.FiniteStateMachine_State.Patrol;
            return;
        }
		//condition check enemy size
        if (enemytoFollow.gameObject.transform.localScale.x  > this.gameObject.transform.localScale.x) {
            currentState = FiniteStateMachine.FiniteStateMachine_State.Run;
        }
        target = enemytoFollow.transform.position;
        transform.position = Vector3.MoveTowards(transform.position, target, speed * Time.deltaTime);
    }
  
    protected void UpdateRunState() {
        if (previousState != currentState) {
            previousState = currentState;
        }
        if (enemytoFollow == null) {
            GetNewPatrolPoint();
            currentState = FiniteStateMachine.FiniteStateMachine_State.Patrol;
            return;
        }
        target = enemytoFollow.transform.position;
        transform.position = Vector3.MoveTowards(transform.position, target, -speed*Time.deltaTime);
    }
    private Transform EnemyIsNear() {
        return enemytoFollow;
    }
    private void Grow() {
        transform.localScale += scaleAdd;
        //adjust speed
        SpeedChange();
    }
    private void SpeedChange() {
        speed -= Scale.x / 10.0f;
    }
    //Collider
    private void OnTriggerEnter(Collider other) {
        if (other.gameObject.transform.localScale.x < this.gameObject.transform.localScale.x) {
            Destroy(other.gameObject);
            Grow();
            GetNewPatrolPoint();
            currentState = FiniteStateMachine.FiniteStateMachine_State.Patrol;
        } else if (other.gameObject.transform.localScale.x > this.gameObject.transform.localScale.x) {
            Destroy(this.gameObject);
            //Grow();
        } else if(other.gameObject.transform.localScale.x == this.gameObject.transform.localScale.x){
			//Do Nothing
		}
    }
}
